//
//  CollectionViewGridLayout.swift
//  ExampleProj
//
//  Created by Ievgen on 2/10/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

class CollectionViewGridLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()

        self.itemSize = CGSize(width: 300, height: 213)
        self.minimumLineSpacing = 10
        self.minimumInteritemSpacing = 10
        self.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.scrollDirection = .horizontal
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
