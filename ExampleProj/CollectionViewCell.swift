//
//  CollectionViewCell.swift
//  ExampleProj
//
//  Created by Ievgen on 2/9/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
}
