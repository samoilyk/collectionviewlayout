//
//  TopicsViewController.swift
//  ExampleProj
//
//  Created by Ievgen on 2/8/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import UIKit

protocol SwitchProtocol: class {
    func switchStateChanged(to state: Bool)
}

class TopicsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layoutSwitcher: UISwitch!

    var coordinator: TopicsCoordinator!
    var viewModel: TopicsViewModel!
    var dataSourceDelegate: TableViewDataSourceDelegate?
    weak var delegate: SwitchProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSourceDelegate = TableViewDataSourceDelegate(dataSource: viewModel.data, layouts: viewModel.layoutsForCollectionView)
        delegate = dataSourceDelegate
        tableView.dataSource = dataSourceDelegate
        tableView.delegate = dataSourceDelegate
        tableView.rowHeight = tableView.frame.height
    }

    @IBAction func switchAction(_ sender: UISwitch) {
        delegate?.switchStateChanged(to: sender.isOn)
    }
}

extension TopicsViewController: Coordinated {

    func setCoordinator(_ coordinator: Coordinator) {
        self.coordinator = coordinator as? TopicsCoordinator
    }

    func getCoordinator() -> Coordinator? {
        return coordinator
    }
}
