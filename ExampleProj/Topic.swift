//
//  Topic.swift
//  ExampleProj
//
//  Created by Ievgen on 2/8/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation

struct Topic {
    var name: String
    var thumbnailsArray: [String]

    init(name: String, numberOfItems: Int) {
        self.name = name
        let array = Array(repeating: "", count: numberOfItems)
        self.thumbnailsArray = array.enumerated().map { "\(name)\($0.offset)" }
    }

}
