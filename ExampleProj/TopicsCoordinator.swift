//
//  TopicsCoordinator.swift
//  ExampleProj
//
//  Created by Ievgen on 2/8/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

class TopicsCoordinator: Coordinator {

    let window: UIWindow
    weak var viewController: TopicsViewController?

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TopicsViewController") as? TopicsViewController
        viewController?.setCoordinator(self)
        viewController?.viewModel = TopicsViewModel()
        window.rootViewController = viewController
    }
}
