//
//  TableViewDataSourceDelegate.swift
//  ExampleProj
//
//  Created by Ievgen on 2/9/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

protocol ChangeLayoutProtocol: class {
    func changeLayout(to layout: LayoutType, animated: Bool)
}

class TableViewDataSourceDelegate: NSObject {

    let layoutsForCollectionView: [LayoutsCollection]
    var dataSource: [Topic]
    var storedOffsets = [Int: CGFloat]()
    var delegate: ChangeLayoutProtocol?

    var layoutType: LayoutType = .flow

    init(dataSource: [Topic], layouts: [LayoutsCollection]) {
        self.dataSource = dataSource
        self.layoutsForCollectionView = layouts
    }

}

extension TableViewDataSourceDelegate: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else { return UITableViewCell() }

        return cell
    }
}

extension TableViewDataSourceDelegate: UITableViewDelegate {

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? TableViewCell else { return }
        delegate = tableViewCell
        tableViewCell.layoutType = layoutType
        tableViewCell.layoutsForCollectionView = layoutsForCollectionView
        tableViewCell.setCollectionViewDataSourceDelegate(to: self, for: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        guard let tableViewCell = cell as? TableViewCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
}

extension TableViewDataSourceDelegate: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        let numberOfItems = dataSource[collectionView.tag].thumbnailsArray.count
        return numberOfItems
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
        let thumbnailName = dataSource[collectionView.tag].thumbnailsArray[indexPath.item]
        cell.thumbnailImageView.image = UIImage(named: thumbnailName)

        return cell
    }
}

extension TableViewDataSourceDelegate: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        guard layoutType == .custom else {
            collectionView.deselectItem(at: indexPath, animated: false)
            return
        }

        let layout = CollectionViewCustomLayout()
        layout.layoutsForCollectionView = layoutsForCollectionView
        collectionView.setCollectionViewLayout(layout, animated: true) { (_) in
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}

extension TableViewDataSourceDelegate: SwitchProtocol {

    func switchStateChanged(to state: Bool) {

        layoutType = (state == true ? .custom : .flow)

        delegate?.changeLayout(to: layoutType, animated: true)
    }
}
