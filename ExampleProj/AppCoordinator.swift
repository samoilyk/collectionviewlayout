//
//  AppCoordinator.swift
//  ExampleProj
//
//  Created by Ievgen on 2/9/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {

    let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let topicsCoordinator = TopicsCoordinator(window: window)
        topicsCoordinator.start()
    }

    func showAuth() {
        // Authentication coordinator initialization
    }
}
