//
//  CollectionViewCustomLayout.swift
//  ExampleProj
//
//  Created by Ievgen on 2/9/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

// Layouts for iPad's Main Screen
class CollectionViewCustomLayout: UICollectionViewLayout {

    let contentWidthConstant: CGFloat = 50
    var layoutsForCollectionView = [LayoutsCollection]()

    private var cache = [IndexPath: UICollectionViewLayoutAttributes]()
    private var contentWidth: CGFloat = 0
    private var contentHeight: CGFloat {
        return collectionView!.bounds.height - (collectionView!.contentInset.top + collectionView!.contentInset.bottom)
    }

    override func prepare() {
        cache.removeAll()
        contentWidth = 0

        let collView = collectionView!
        var xOffset: CGFloat = 0

        for section in 0 ..< collView.numberOfSections {
            var numberOfItems = collView.numberOfItems(inSection: section)
            numberOfItems > 20 ? (numberOfItems = 20) : (numberOfItems = numberOfItems)

            var x: CGFloat = 0
            var y: CGFloat = 0
            var width: CGFloat = 0
            var height: CGFloat = 0
            var zIndexCoeficient = 1
            var counter = 0

            for i in 0 ..< collView.numberOfItems(inSection: section) {
                if counter == 20 {
                    counter = 0
                    xOffset = x + width
                }

                let indexPath = IndexPath(item: i, section: section)
                let indexPathSelected = collView.indexPathsForSelectedItems?.first

                if indexPath == indexPathSelected {
                    width = 600
                    height = 425
                    x = layoutsForCollectionView[numberOfItems-1].layoutsArray[counter].x + xOffset + 300/2
                    y = (collView.frame.height - height) / 2

                    zIndexCoeficient = 100
                    xOffset = xOffset + width
                } else {
                    width = 300
                    height = 213
                    x = layoutsForCollectionView[numberOfItems-1].layoutsArray[counter].x + xOffset
                    y = layoutsForCollectionView[numberOfItems-1].layoutsArray[counter].y
                    zIndexCoeficient = 1
                }

                // Frame for item
                let frame = CGRect(x: x, y: y, width: width, height: height)

                // Adding atributes to the cache
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                attributes.zIndex = (counter + 1) * zIndexCoeficient
                cache[indexPath] = attributes

                // Width for collectionViewContentSize
                contentWidth = max(contentWidth, frame.maxX)

                counter += 1
            }

            // padding between sections
            xOffset = x + width + 150
        }
    }

    override var collectionViewContentSize: CGSize {
        return CGSize(width: (contentWidth + contentWidthConstant), height: contentHeight)
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath]
    }

    // Getting atributes from cache for visible rect
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = cache.filter { $0.value.frame.intersects(rect) }.map { $0.value }

        return layoutAttributes
    }
}
