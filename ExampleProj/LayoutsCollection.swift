//
//  LayoutsCollection.swift
//  ExampleProj
//
//  Created by Ievgen on 2/9/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

struct LayoutForCell {

    var x: CGFloat
    var y: CGFloat
    var width: CGFloat
    var height: CGFloat

    init(x: CGFloat, yMultiplicator: CGFloat, width: CGFloat) {
        let contentHeight = UIScreen.main.bounds.height - 60
        let const = contentHeight/370
        self.width = width * (const)
        self.height = self.width * 9/16
        self.x = x * const
        self.y = (yMultiplicator * (contentHeight - 2 * 80 * const)) + 80 * const - (self.height / 2)
    }
}

struct LayoutsCollection {
    var layoutsArray = [LayoutForCell]()
}
