//
//  TopicsViewModel.swift
//  ExampleProj
//
//  Created by Ievgen on 2/8/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import Foundation
import UIKit

class TopicsViewModel {

    var layoutsForCollectionView = [LayoutsCollection]()
    var data = [Topic]()

    init() {
        self.data = someTopics()
        self.layoutsForCollectionView = createLayouts()
    }
}

extension TopicsViewModel {

    func someTopics() -> [Topic] {
        let topics = [Topic(name: "music", numberOfItems: 16),
                      Topic(name: "sport", numberOfItems: 16),
                      Topic(name: "food", numberOfItems: 16),
                      Topic(name: "nature", numberOfItems: 16)]
        return topics
    }

    func createLayouts() -> [LayoutsCollection] {

        var layoutsToReturn = [LayoutsCollection]()

        guard let path = Bundle.main.path(forResource: "layouts", ofType: "json") else { return layoutsToReturn }
        let url = URL(fileURLWithPath: path)
        if let data = try? Data(contentsOf: url) {
            let json = JSON(data: data)

            for i in 0..<json.count {
                var layouts = LayoutsCollection()
                for j in 0...i {

                    let x = CGFloat(json["\(i+1)"][j]["x"].intValue)
                    let y = CGFloat(json["\(i+1)"][j]["y"].floatValue)
                    let width = CGFloat(json["\(i+1)"][j]["width"].floatValue)
                    let layout = LayoutForCell(x: x, yMultiplicator: y, width: width)

                    layouts.layoutsArray.append(layout)
                }
                layoutsToReturn.append(layouts)
            }
        }
        return layoutsToReturn
    }
}
