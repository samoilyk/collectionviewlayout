//
//  TableViewCell.swift
//  ExampleProj
//
//  Created by Ievgen on 2/9/17.
//  Copyright © 2017 Ievgen. All rights reserved.
//

import UIKit

enum LayoutType {
    case flow
    case custom
}

class TableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var layoutType: LayoutType = .flow
    var layoutsForCollectionView = [LayoutsCollection]()
    
    var collectionViewOffset: CGFloat {
        get {
            return collectionView.contentOffset.x
        }

        set {
            collectionView.contentOffset.x = newValue
        }
    }

    func setCollectionViewDataSourceDelegate(to dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, for row: Int) {

        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.backgroundView?.isUserInteractionEnabled = false
        collectionView.allowsMultipleSelection = false
        collectionView.reloadData()
        changeLayout(to: layoutType, animated: false)
    }
}

extension TableViewCell: ChangeLayoutProtocol {

    func changeLayout(to layout: LayoutType, animated: Bool) {
        var layoutToSet = UICollectionViewLayout()

        let gridLayout = CollectionViewGridLayout()
        let customLayout = CollectionViewCustomLayout()
        let layouts = layoutsForCollectionView
        customLayout.layoutsForCollectionView = layouts
        
        layoutToSet = (layout == .custom ? customLayout : gridLayout)
        collectionView.setCollectionViewLayout(layoutToSet, animated: animated)
    }
}
